clear; close all;

s=tf('s');
t=0.01*(1:500);

G0=2;
G1=5/(s^2+2*s+5);
G2=2/(s+2);

r1=step(G1,t);
r2=step(G2,t);
r3=step(G1*G2,t);
r4=step(G0*G1*G2,t);

[B,A]=tfdata(G1*G2);
A=A{1};B=B{1};

[r,p,k] = residue(B,A);

figure; hold on;
plot([r1,r2,r3,r4])
legend(["G1";"G2";"G1·G2";"G0·G1·G2"]);
