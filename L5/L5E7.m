clear; close all;

s=tf('s');
t=0.01*(1:500);


Ga=2.1/(s^2+2*s+2.1);
Gb=2/(s+1);
Gc=Gb;

F=feedback(Ga*Gb,Gc);

r1=step(F);


figure;
plot(r1);
pzmap(F)
