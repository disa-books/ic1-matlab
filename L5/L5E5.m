clear; close all;

s=tf('s');
t=0.01*(1:500);


G0=1000/(s^2+2*s+5);
G1=(s+7)/(s+8);
G2=1/(s^2+20*s+125);

G=G0*G1*G2;

r1=step(G,t);

G=7/(s^2+2*s+5);

r2=step(G,t);

figure; hold on;
plot(r1);
plot(r2,'--');
legend(['G';'G~']);

G0=300/(s^2+2*s+5);
G1=(s+6)/(s+8);
G2=1/(s^2+20*s+125);

G=G0*G1*G2;

r1=step(G,t);

G1=6/8;
G=2.4*G1/(s^2+2*s+5);

r2=step(G,t);

figure; hold on;
plot(r1);
plot(r2,'--');
legend(['G';'G~']);




