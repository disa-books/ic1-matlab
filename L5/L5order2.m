clear; close all;

s=tf('s');
t=0.01*(1:500);

%w0=3;
%G0=w0/(s+w0);
%A=6*w0

p1=-1+1i;p2=conj(p1);%original
G0=(p1*p2)/((s^2-(p1+p2)*s+p1*p2));
A=6*sqrt(p1*p2)


p1=-5+50i;p2=conj(p1);%high wd, low sigma
G1=(p1*p2)/((s^2-(p1+p2)*s+p1*p2));
wn=sqrt(p1*p2)

p1=-50+5i;p2=conj(p1);%high sigma, low wd
G2=(p1*p2)/((s^2-(p1+p2)*s+p1*p2));
wn=sqrt(p1*p2)

wd=imag(p1);
sig=-real(p1);



r0=step(G0,t);
r1=step(G1,t);
r2=step(G2,t);
r3=step(G0*G1,t);
r4=step(G0*G2,t);

figure; hold on;
plot([r0],'.')
plot([r1,r2,],'--')
plot([r3,r4],'*')
legend(["G0";"G1";"G2";"G0·G1";"G0·G2"]);


