clear; close all;

s=tf('s');
t=0.01*(1:500);

G0=2;
%G0=(s+15)/15;
G1=5/(s^2+2*s+5);
G2=s/s;
%G2=2/(s+2);

%p1=-1+10i;p2=conj(p1);%high wd, low sigma
p1=-10+1i;p2=conj(p1);%high sigma, low wd

%G1=(p1*p2)/((s^2-(p1+p2)*s+p1*p2));

wn=sqrt(p1*p2);
wd=imag(p1);
sig=-real(p1);

%p=6*sig;
%p=6*wd;
%p=6*wn;
%G2=p/(s+p);

r1=step(G1,t);
r2=step(G2,t);
r3=step(G1*G2,t);
r4=step(G0*G1*G2,t);

figure; hold on;
plot([r1,r2,r3,r4])
legend(["G1";"G2";"G1·G2";"G0·G1·G2"]);


