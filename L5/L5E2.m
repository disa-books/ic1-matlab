clear; close all;

s=tf('s');
t=0.01*(1:500);

G0=1%0.98;
G0=(s+15)/15;
G1=10/((s+4)^2+9); %gain NOT 1 !!
G2=(s+5)/(s+5.0);


p1=-4+3i;p2=conj(p1);
G1=10/((s^2-(p1+p2)*s+p1*p2));

wn=sqrt(p1*p2);
wd=imag(p1);
sig=-real(p1);


r1=step(2*G1,t);
r2=step(1*G2,t);
r3=step(2*G1*G2,t);
r4=step(2*G0*G1,t);

figure; hold on;
plot([r1,r2,r3,r4])
legend(['G1';'G2';'G1·G2';'G1·G0']);


