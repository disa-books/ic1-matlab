clear;close all;
clf;
s=tf('s');
%'pkg load control'

% 
% Dibujar el lugar de las raíces para el sistema 
% GH =( (s+1)/( (s-2)*(s+3) ) ) * ( 1/(s*(s+4)) )


%G = 
numG=[1 1];    %  (s+1)
denG=[1 1 -6]; %  (s-2)*(s+3)

G=tf( numG , denG );

numH=  1 ; %   1
denH= [1 4 0]; % (s*(s+4))

H=tf( numH , denH);

GH = H*G

fig= figure; rlocus(GH);
grid on;

%manually print root locus for k actual value
N=conv( numH , numG );
D=conv( denH , denG );


hold on;
for k=0:100
w = waitforbuttonpress;

pause(0.1);
%Teniendo D y N para el lazo abierto, la función de transferencia
%será CL = (k*N/D)/(1+k*N/D) = k*N/(D+k*N)
N=[zeros(1,size(D,2)-size(N,2)), N]; %zero pad array N para D+k*N
P=D+k*N;

%poles= roots(P);p1=poles(1);p2=poles(2);
%Se puede hacer con roots, pero vamos a ver el cómo afecta el signo a RL
%usando la solución de segundo grado z=(-b+sqrt(b^2-4ac))/2a.
ps=roots(P);

%plot pole
p=ps;
p=p+1i*10^-10; %add tiny complex i for correct plot of real only values.
plot(p,'p');
legend(num2str(ps));

end
% 
% 


