clear;
close all;

figure;hold on;

w=1:9
ww=[10^-2*w,10^-1*w,10^0*w,10^1*w,10^2*w]

os=[+1];
ps=[3*i -3*i];

for w=ww

    s=1i*w;
    
    %G=s;
    G=prod(s-os)./prod(s-ps);
    plot(G,'.');
    text(real(G),imag(G),string(w));
    mag=20*log(abs(G));
    phi=angle(G)*180/pi;

end


nyquist(zpk(os,ps,1))
figure;
bode(zpk(os,ps,1))




