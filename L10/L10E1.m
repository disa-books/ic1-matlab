clear;
close all;

figure;hold on;

w=1:9;
ww=[10^-2*w,10^-1*w,10^0*w,10^1*w,10^2*w];

os=[-2];
ps=[-1 -3];

for w=ww

    s=1i*w;

    %G=s;
    G=prod(s-os)./prod(s-ps);
    plot(G,'.');
    text(real(G),imag(G),num2str(w));
    mag=20*log(abs(G));
    phi=angle(G)*180/pi;
    xlim([0 1]);
    ylim([-0.5 0.5]);
    waitforbuttonpress;

end


nyquist(zpk(os,ps,1))
xlim([0 1]);
ylim([-0.5 0.5]);
figure;
bode(zpk(os,ps,1))




