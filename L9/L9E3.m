clear;close all;

s=tf('s');

G=(s+2)/((s^2+s+4)*(s+1)*s);

bode(G);

jw=1i;

G=(jw+2)/((jw^2+jw+4)*(jw+1)*jw);

20*log10(abs(G))