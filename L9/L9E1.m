clear;close all;

%s=tf('s');
ds=0.05;

sig=-10:ds:10;
wd=-10:ds:10;

s=sig'+1i*wd;

%G=s;
G=1./((s+1).*(s+2));
mag=20*log(abs(G));
phi=angle(G)*180/pi;

surf(sig,-wd,mag);
%bode(G);

at=find(sig==0);

figure;
%plot(wd,mag(at,:));
%semilogx(wd,mag(at,:));
semilogx(wd,phi(at,:));

