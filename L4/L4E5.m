syms k

b1=(10*k-430)/14;
c1=( 14*(3*k+4500)-b1*(4*k-900) ) / -b1;

sol=solve(c1==0,k);
k=double(sol(2))

syms s;

Df=s^4+14*s^3+(k-95)*s^2+(4*k-900)*s+3*k+4500

sol=solve(Df==0,s)
double(sol)