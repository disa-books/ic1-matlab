clear;close all;
s=tf('s');

G=4/((s+3)*(s+1));
H=2/((s+6));

GH=G*H;

%%PD
o1=-5.91;k=1.67;
R=k*(s-o1);


%%Section 4
%o1=-3.4;o2=-3.4;k=3.586;
%o1=-12.64;o2=-2.09/6;k=1.13;
%R=k*(s-o1)*(s-o2)/s;

F=feedback(R*G,H);
[Nn,Dn]=tfdata(F);
roots(Dn{1})

figure;
step(F,10);
figure;
rlocus(R*GH);


