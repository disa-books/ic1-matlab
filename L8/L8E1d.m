clear;close all;
s=tf('s');

GH=1/((s+1)*(s+2));

%%Section 1
%R=6.5;

%%Section 2
%o1=-0.25;k=6.5;
%R=k*(s-o1)/s;

%%Section 3
%o1=-14.24;k=1.22;
%R=k*(s-o1);


%%Section 4
%o1=-3.4;o2=-3.4;k=3.586;
o1=-12.64;o2=-2.09/6;k=1.13;
R=k*(s-o1)*(s-o2)/s;

F=feedback(R*GH,1);
[Nn,Dn]=tfdata(F);
roots(Dn{1})

figure;
step(F,10);
figure;
rlocus(R*GH);


