clear;close all;
s=tf('s');
%#ok<*NOPTS>

GH=3*(s+8)/((s+1)*(s+4)*(s+11));
%GH=1/( (s+2)*(s+5)*(s+0.8) );
[zs,ps,k]=zpkdata(GH);
zs=zs{1};ps=ps{1};
%rlocus(GH)

%sig=3.2;
sig=3.9;

theta=atan(-pi/log(0.15));
wd=tan(theta)*sig

pd=-sig+1i*wd

ang = sum(angle(pd-zs))-sum(angle(pd-ps));

[ angle(pd-ps)' ,"-> suma", ang]

abs(pd-ps)
kp = prod(abs(pd-ps))/prod(abs(pd-ps));
kp=kp/k;

rlocus (GH);
hold on;
plot(pd,'*');



