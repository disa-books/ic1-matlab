clear;close all;
clf;
s=tf('s');
%'pkg load control' en octave

%
% Dibujar el lugar de las raíces para el sistema
% GH =( (s+1)/( (s-2)*(s+3) ) ) * ( 1/(s*(s+4)) )


%G =
numG=[1 -1];    %  (s-1)
denG=[1 2 2]; %  (s^2+2*s+2)

G=tf( numG , denG );

numH=  1 ; %   1
denH= [1]; % (s*(s+4))

H=tf( numH , denH);

GH = H*G

fig= figure; rlocus(GH);
grid on;

%manually print root locus for k actual value
N=conv( numH , numG );
D=conv( denH , denG );


hold on;
for k=0:-1:-50
%w = waitforbuttonpress;

pause(0.1);
%Teniendo D y N para el lazo abierto, la función de transferencia
%será CL = (k*N/D)/(1+k*N/D) = k*N/(D+k*N)
N=[zeros(1,size(D,2)-size(N,2)), N]; %zero pad array N para D+k*N
P=D+k*N;

%Se puede hacer con roots.
ps=roots(P);

%plot pole
p=ps;
p=p+1i*10^-10; %add tiny complex i for correct plot of real only values.
plot(p,'.');
legend(num2str(ps));

end
%
%


