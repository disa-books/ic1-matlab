clear;
close all;

N=1000;
dTs=0.01;
t=dTs*(1:N);

f=1* exp(-t); %e^-t laplace 1/(s+1)

figure;
plot(t,f)

%% convolution step input

in=1*ones(1,N); %step laplace -> 1/s
figure;
plot(t,in);


out=conv(f,in);
% see https://math.stackexchange.com/questions/1318672/convolution-in-matlab-with-different-sampling
out=dTs*out(1:N);

figure;
plot(t,out);

%% linear system tools

s=tf('s');
figure;
step(1/(s+1))