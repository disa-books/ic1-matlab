# ic1-matlab



## Name
Matlab scripts for the control engineering course.

## Description
Scripts solving the control engineering course exercises.

## Visuals


## Usage
Download the scripts and run using matlab/octave.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
This is a work in progress. There will be more exercises solved in the future.

## Contributing
If you find any errors, please create a new issue.



