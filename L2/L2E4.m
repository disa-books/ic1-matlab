clear;close all;

u=0;

sol= fsolve(@eq,[1 1 -1]);
% eq([3 3 3 3])

%% ode45

T=10;
t=0:0.01:T;

x0=[0 0]';
v=1;

[t,y] = ode45( @(t,x)L2E4f(t,x,v),t,x0);

plot(t,y)

%% functions

function os=eq(var)

x=var(1);
y=var(2);
z=var(3);
in=0;

os(1)=sqrt(x)+x*z+2*in;
os(2)=x*y*z+3;
os(3)=x+z;

end