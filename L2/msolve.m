clear;
T=10;
t=0:0.01:T;

x0=[0]';
v=0.5;

[t,y] = ode45( @(t,x)motor_ode(t,x,v),t,x0);
%motor_ode(1,[1 1 ;1 1 ;1 1])
plot(t,y)

s=tf('s');

G=-0.42/(s+1.53);

figure;
step(0.5*G);