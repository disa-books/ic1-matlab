clear;close all;
%#ok<*SAGROW> 

dTs=0.01;
N=1000;

t=dTs*(1:N);

% x=0.5*ones(1,N);
% x=sin(1*t);
x=log(t);

% xd(1)=(x(1)-0)/dTs;
xd(1)=0;
for i=2:N
    xd(i)=(x(i)-x(i-1))*dTs; 
end

y=3*x.^2+4*xd.*x+sin(x)+2;

figure;hold on;

plot(t,x);
plot(t,3*x.^2);
plot(t,4*xd.*x);
plot(t,sin(x));
legend(["x";"3x²";"4·x·xd";"sin x"])

figure;
plot(t,y);

figure;grid on;
plot(x,y,'*');
% xlim([-1,1]);ylim([0,6]);

%% Linearize



