clear; close all;

dx=1;
dy=1;

x=0:dx:15;
y=0:dy:15;

z=x'*y;

surf(x,y,z);


hold on;
X=meshgrid(x-6);
Y=meshgrid(y-11)';
zl=66+11*X+6*Y;

surf(x,y,zl);


% xlim([5 7]);
% ylim([10 12]);